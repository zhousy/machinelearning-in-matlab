function T=id3tree(xTr,yTr,maxdepth,weights)
% function T=id3tree(xTr,yTr,maxdepth,weights)
%
% The maximum tree depth is defined by "maxdepth" (maxdepth=2 means one split). 
% Each example can be weighted with "weights". 
%
% Builds an id3 tree
%
% Input:
% xTr | dxn input matrix with n column-vectors of dimensionality d
% xTe | dxm input matrix with n column-vectors of dimensionality d
% maxdepth = maximum tree depth
% weights = 1xn vector where weights(i) is the weight of example i
% 
% Output:% T = decision tree
    %creat T
    %xTr1=xTr;
    %computing level 1
    [d,n]=size(xTr);
    if nargin<4
        weights=ones(1,n)./n;
        maxdepth=inf;
        %maxdepth=5;
    end;
    [feature,cut,~]=entropysplit(xTr,yTr,weights);
    leftT=[xTr(:,xTr(feature,:)<=cut)];
    rightT=[xTr(:,xTr(feature,:)>cut)];
    [~,nl]=size(leftT);
    [~,nr]=size(rightT);
    T=[mode(yTr);feature;cut;2;3;0];
    if nl==0||nr==0
        T(4,1)=0;
        T(5,1)=0;
    end
    flag=ones(1,n);
    head=1;tail=1;
    pp=0;
    while true
        num=0;
        for j=head:tail
            if T(4,j)~=0
              num=num+1;
            end
        end
        pp=pp+1;
        ind=tail;
        kk=0;
        for j=head:tail
            temp=xTr(:,find(flag(j,:)==1));
            tempy=yTr(:,find(flag(j,:)==1));
            tempw=weights(:,find(flag(j,:)==1));
            if T(4,j)~=0
                templ=temp(:,temp(T(2,j),:)<=T(3,j));
                temply=tempy(:,temp(T(2,j),:)<=T(3,j));
                templw=tempw(:,temp(T(2,j),:)<=T(3,j));
                [feature,cut,~]=entropysplit(templ,temply,templw);
                flag=[flag;zeros(1,n)];
                if feature==0
                    Tt=[mode(temply);feature;cut;0;0;j];
                    T=[T Tt];
                    ind=ind+1;
                else
                    leftT=templ(:,templ(feature,:)<=cut);
                    rightT=templ(:,templ(feature,:)>cut);
                    [~,nl]=size(leftT);
                    [~,nr]=size(rightT);
                    Tt=[mode(temply);feature;cut;tail+2*num+1+kk;tail+2*num+2+kk;j];
                    kk=kk+2;
                    ind=ind+1;
                    T=[T Tt];
                    if nl==0||nr==0
                        T(4,ind)=0;
                        T(5,ind)=0;
                    else
                        %flag=[flag;zeros(1,n)];
                        ind1=find(temp(T(2,j),:)>T(3,j));
                        ind2=find(flag(j,:)==1);
                        ind2(ind1)=[];
                        flag(ind,ind2)=1;
                    end
                end
                tempr=temp(:,temp(T(2,j),:)>T(3,j));
                tempry=tempy(:,temp(T(2,j),:)>T(3,j));
                temprw=tempw(:,temp(T(2,j),:)>T(3,j));
                [feature,cut,~]=entropysplit(tempr,tempry,temprw);
                flag=[flag;zeros(1,n)];
                if feature==0
                    Tt=[mode(tempry);feature;cut;0;0;j];
                    T=[T Tt];
                    ind=ind+1;
                else
                    leftT=tempr(:,tempr(feature,:)<=cut);
                    rightT=tempr(:,tempr(feature,:)>cut);
                    [~,nl]=size(leftT);
                    [~,nr]=size(rightT);
                    %if ind==tail
                    Tt=[mode(tempry);feature;cut;tail+2*num+1+kk;tail+2*num+2+kk;j];
                    kk=kk+2;
                    %else
                    %    Tt=[mode(tempry);feature;cut;ind+2*num+2;ind+2*num+3;j];
                    %end;
                    %T=[T Tt];
                    ind=ind+1;
                    T=[T Tt];
                    if nl==0||nr==0
                        T(4,ind)=0;
                        T(5,ind)=0;
                    else
                        %flag=[flag;zeros(1,n)];
                        ind1=find(temp(T(2,j),:)<=T(3,j));
                        ind2=find(flag(j,:)==1);
                        ind2(ind1)=[];
                        flag(ind,ind2)=1;
                    end
                end
            end
        end
        if pp==maxdepth
            for j=(tail+1):ind
               T(4,j)=0;
               T(5,j)=0;
            end
            break;
        end
        if kk==0
            break;
        end;
        head=tail+1;
        tail=ind;
    end


