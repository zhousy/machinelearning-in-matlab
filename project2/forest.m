function F=forest(x,y,nt)
% function F=forest(x,y,nt)
%
% INPUT: 
% x | input vectors dxn
% y | input labels 1xn
%
% OUTPUT:
% F | Forest
%

%% fill in code here
F=[];
[d,n]=size(x);
k=ceil(sqrt(d));
%randK=randperm(d);
w=ones(1,n)*(1/n);
for i=1:nt
    randK=randperm(d);randK=(randK(1:k));
    c=round((n-1)*rand(1,n))+1;
    xP=x(:,c);
    yP=y(c);
    Tt=id3tree(xP,yP,inf,w);
    xx=x;
    c=unique(c);
    xx(:,c)=[];
    yy=y;
    yy(:,c)=[];
    Tt=prunetree(Tt,xx,yy);
    F=[F Tt];
end