function BDT=boosttree(x,y,nt,maxdepth)
% function BDT=boosttree(x,y,nt,maxdepth)
%
% Learns a boosted decision tree on data x with labels y.
% It performs at most nt boosting iterations. Each decision tree has maximum depth "maxdepth".
%
% INPUT: 
% x  | input vectors dxn
% y  | input labels 1xn
% nt | number of trees (default = 100)
% maxdepth | depth of each tree (default = 3)
%
% OUTPUT:
% BDT | Boosted DTree
%

%% fill in code here
n=size(y,2);
H=[];
w=ones(1,n).*(1/n);
for t=1:nt
    h=id3tree(x,y,maxdepth,w);
    yE=evaltree(h,x);
    ep=sum(w(find(yE~=y)));
    if ep>0.5 
        break;
    end
    alph=0.5*log((1-ep)/ep);
    fac=2*sqrt(ep*(1-ep));
    for i=1:n
        %if yE(i)==y(i)
            w(i)=w(i)*exp(-1*alph)/fac;
        %else
        %   w(i)=w(i)*exp(1*alph)/fac;
        %end
    end
    H=h;
end
BDT=H;
