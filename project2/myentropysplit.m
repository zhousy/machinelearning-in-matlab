function [feature,cut]=myentropysplit(xTr,yTr)

[d,n]=size(xTr);
[~,u,yTri]=unique(yTr);
l=length(u);
weights=ones(1,n).*(1/n);
feature=0;cut=0;bestgain=Inf;
for i=1:d
    [num,inde]=sort(xTr(i,:));
    wei=weights(inde);
    y=yTri(inde)';
    yy=zeros(n,l);
	for j=1:n
        yy(j,y(j))=weights(j);
	end;
    p=bsxfun(@times,(yy),1./cumsum(wei'));
    q=-p.*log2(p);
    q(isnan(a))=0;				
	q=bsxfun(@times,q,cumsum(wei'));
    pp=bsxfun(@times,cumsum(yy(end:-1:1,:)),1./cumsum(wei(end:-1:1)'));
	qq=-pp.*log2(pp);
	qq(isnan(qq))=0;
	qq=bsxfun(@times,qq,cumsum(wei(end:-1:1)'));
	qq=qq(end-1:-1:1,:);
    diffe=find(abs(diffe(num))>eps*100);
	H=sum(q(diffe,:)+qq(diffe,:),2);
    if isempty(H)
        continue;
    end
	[bestg,inde2]=min(H);
    if bestg<bestgain
		cut=mean([num(diffe(inde2)) num(diffe(inde2)+1)]);		    
		feature=i;
		bestgain=bestg;
    end
end