function T=prunetree(T,xTe,y)
% function T=prunetree(T,xTe,y)
%
% Prunes a tree to minimal size such that performance on data xTe,y does not
% suffer.
%
% Input:
% T = tree
% x = validation data x (dxn matrix)
% y = labels (1xn matrix)
%
% Output:
% T = pruned tree 
%

%% fill in code here
[~,q]=size(T);
[~,n]=size(xTe);
Tp=zeros(1,q);
flag=zeros(1,q);
ypre=evaltree(T,xTe);
acc1=length(find((y-ypre)==0))/n;
Tp(1,:)=acc1;
for i=q:-1:1
    if (T(6,i)~=0)&&(flag(T(6,i))~=1)
        Tt=T;
        Tt(4,T(6,i))=0;
        Tt(5,T(6,i))=0;
        ypre=evaltree(Tt,xTe);
        acc2=length(find((y-ypre)==0))/n;
        k=T(6,i);
        if acc2>=Tp(k)
            flag(k)=1;
            while k>0
                if Tp(k)<=acc2
                    Tp(k)=acc2;
                end
                k=T(6,k);
            end
        end
    end
end
%aaa=1;
TT=T;
for i=q:-1:1
    if (flag(i)==1)&&(T(6,i)~=0)
        T(4,i)=0;
        T(5,i)=0;
    end
end
%bb=1;
