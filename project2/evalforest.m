function preds=evalforest(F,xTe)
% function preds=evalforest(F,xTe);
%
% Evaluates a random forest on a test set xTe.  
%
% input: 
% F   | Forest of decision trees
% xTe | matrix of m input vectors (matrix size dxm)
%
% output: 
%
% preds | predictions of labels for xTe
%

%% fill in code here
[~,ntn]=size(F);
head=0;tail=0;
ypre=[];
while true
    head=tail+1;
    tail=tail+2;
    while (F(6,tail)~=0)&&(tail<ntn)
        tail=tail+1;
    end;
    if tail>=ntn
        tail=ntn;
    else
        tail=tail-1;
    end;
    T=F(:,head:tail);
    ypre=[ypre;evaltree(T,xTe)];
    if tail==ntn
        break;
    end;
end

%%process the output
preds=mode(ypre);
%preds=round(mean(ypre));
