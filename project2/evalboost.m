function preds=evalboost(BDT,xTe)
% function preds=evalboost(BDT,xTe);
%
% Evaluates a boosted decision tree on a test set xTe.  
%
% input: 
% BDT | Boosted Decision Trees
% xTe | matrix of m input vectors (matrix size dxm)
%
% output: 
%
% preds | predictions of labels for xTe
%
preds=evaltree(BDT,xTe);
%% fill in code here
% [~,ntn]=size(BDT);
% F=BDT(2:7,:);
% head=0;tail=0;
% ypre=[];
% al=[];
% while true
%     head=tail+1;
%     tail=tail+2;
%     while (F(6,tail)~=0)&&(tail<ntn)
%         tail=tail+1;
%     end;
%     if tail>=ntn
%         tail=ntn;
%     else
%         tail=tail-1;
%     end;
%     T=F(:,head:tail);
%     al=[al BDT(1,head)];
%     ypre=[ypre;evaltree(T,xTe)];
%     if tail==ntn
%         break;
%     end;
% end
% 
% [num,n]=size(ypre);
% ylable=unique(BDT(2,:));
% ynum=size(ylable,2);
% flag=zeros(ynum,n);
% for i=1:num
%     for j=1:n
%         for k=1:ynum
%             if ylable(k)==ypre(i,j)
%                 flag(k,j)=flag(k,j)+al(i);
%             end
%         end
%     end
% end
% [~,re]=max(flag);
% %%process the output
% preds=ylable(re);

