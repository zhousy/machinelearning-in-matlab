function [ypredict]=evaltree(T,xTe)
% function [ypredict]=evaltree(T,xTe);
%
% input: 
% T0  | tree structure
% xTe | Test data (dxn matrix)
%
% output: 
%
% ypredict : predictions of labels for xTe
%

[~,n]=size(xTe);
ypredict=[];
for i=1:n
    x=xTe(:,i);
    j=1;
    pred=0;
    while j~=0
        pred=T(1,j);
        if T(2,j)~=0
            if x(T(2,j))<=T(3,j)
                j=T(4,j);
            else
                j=T(5,j);
            end
        else
            break;
        end
    end
    ypredict(i)=pred;
end


