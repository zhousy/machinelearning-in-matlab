function [xTr,xTe,u,m]=preprocess(xTr,xTe);
% function [xTr,xTe,u,m]=preprocess(xTr,xTe);
%
% Preproces the data to make the training features have zero-mean and
% standard-deviation 1
%
% output:
% xTr - pre-processed training data 
% xTe - pre-processed testing data
% 
% u,m - any other data should be pre-processed by x-> u*(x-m)
%

% [d,n] = size(xTr);
% temp = ones(1, n);
% m = mean(xTr, 2);
% m = m * temp;
% u = std(xTr, 0, 2);
% s = u * temp;
% xTr = (xTr - m) ./ s;
% 
% [d,n] = size(xTe);
% temp = ones(1, n);
% m2 = mean(xTe, 2);
% m2 = m2 * temp;
% u2 = std(xTe, 0, 2);
% s = u2 * temp;
% xTe = (xTe - m2) ./ s;



m = mean(xTr, 2);
u = diag(1 ./ std(xTr, 0, 2));
xTr = u * (xTr - m * ones(1, length(xTr)));
% m = mean(xTe, 2);
% u = diag(1 ./ std(xTe, 0, 2));
xTe = u * (xTe - m * ones(1, length(xTe)));
xTr = decor(xTr);
xTe = decor(xTe);
% x = [xTr, xTe];
% m = mean(x, 2);
% u = diag(1 ./ std(x, 0, 2));
% x = u * (x - m * ones(1, length(x)));
% xTr = x(:,1:length(xTr));
% xTe = x(:,length(xTr) + 1 : length(x));

% u = eye(d, d);
%% de-correlation

%%

%% de-correlation
function x = decor(x)
 [dd,nn]=size(x);
 m=mean(x,2);
 rpmt=repmat(m,1,nn);
 x=x-rpmt;
 cov=pcacov(x');
 xx=cov'*(x);
 x=cov*xx+rpmt;
%%