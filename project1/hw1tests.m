function [r,s]=hw1tests()
% function [r,s]=hw1tests()
%
% Tests the functions from homework assignment 0
% Please make sure that the error statements are instructive. 
%
% Output: 
% r=0 number of tests that broke
% s= statement describing the failed test (s={} if all succeed)
%


% Put in any seed below
rand('seed',27449002861);
% initial outputs
r=0;
s={};

% Example:
% A simple test is to look if the diagonal is all zero
x=rand(10,100); 		% define some random test points
d=diag(l2distance(x));	% extract diagonal of distance matrix
if ~all(d<eps*100)			% if test failed ...
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='Distance from a vector to itself is not zero.';
end;

x=[[1,0];[0,1]];
z=[[0,0];[1,1]];
D=l2distance(x,z);
D2=[[sqrt(2),sqrt(2)];[0,0]];
if ~(max(max(abs(D2-D)))<100*eps)
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='Distances between xor vectors are not correct.';
end;

% Throw friendly errors if input sizes are wrong
try
	x = rand(10, 5);
	z = rand(5, 10);
	innerproduct(x, z);
	findknn(x, z);
catch
	if length(strfind(lasterr, 'operator *: nonconformant arguments')) ~= 0
		r = r+1;
		s{length(s)+1} = 'Internal error thrown on nonconformant arguments';
	end
end

% Distance function should be commutative (with a transpose)
x1 = rand(10,100);
x2 = rand(10,100);
D1 = l2distance(x1,x2);
D2 = l2distance(x2,x1);
if ~all(all(abs(D1 - D2') < eps*100))
	r = r+1;
	s{length(s)+1} = 'Distance function is not commutative';
end

% Practical tests for analyze and knnclassifier
tmp = magic(8);
x1 = tmp(:,1:4);
x2 = tmp(:,5:8);
y1 = [1 1 2 2];
y2 = knnclassifier(x1, y1, x2, 3);
y2true = [2 2 1 1];
if y2 ~= y2true
	r = r+1;
	s{length(s)+1} = 'Vectors of the magic matrix misclassified';
elseif abs(analyze('acc', y2, y2true)-1) > eps*100
	r = r+1;
	s{length(s)+1} = 'Analyzer function incorrect for identical vectors';
end