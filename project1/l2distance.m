function D=l2distance(x1,x2)
% function D=l2distance(x1,x2)
%	
% Computes the Euclidean distance matrix. 
% Syntax:
% D=l2distance(x1,x2)
% Input:
% x1: dxn data matrix with n vectors (columns) of dimensionality d
% x2: dxm data matrix with m vectors (columns) of dimensionality d
%
% Output:
% Matrix D of size nxm 
% D(i,j) is the Euclidean distance of x1(:,i) and x2(:,j)
%
% call with only one input:
% l2distance(x1)=l2distance(x1,x1)
%

if (nargin==1) % case when there is only one input (x1)
	%% fill in code here
    %if (size(x1,1)==1)
        %x1=[x1;zeros(1,size(x1,2))];
    %end
    %V=x1-x1;
    %D=sqrt(V'*V);
    D=l2distance(x1,x1);    
    D=D.*(1-eye(size(D)));
    %
else  % case when there are two inputs (x1,x2)
	%% fill in code here
    if (size(x1,1)==1)
        x1=[x1;zeros(1,size(x1,2))];
        x2=[x2;zeros(1,size(x2,2))];
    end
    %D=sqrt(((x1').^2)*ones(size(x2))+(ones(size(x1'))*x2.^2)-2*x1'*x2);
    x1x1=sum(x1.*x1);
    x2x2=sum(x2.*x2);
    x1x2=(x1')*x2;
    D=sqrt(repmat(x1x1',[1 size(x2x2,2)])+repmat(x2x2,[size(x1x1,2) 1])-2*x1x2);
    D=real(D);  
end
%



