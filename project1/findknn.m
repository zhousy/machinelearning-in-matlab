function [indices,dists]=findknn(xTr,xTe,k);
% function [indices,dists]=findknn(xTr,xTe,k);
%
% Finds the k nearest neighbors of xTe in xTr.
%
% Input:
% xTr = dxn input matrix with n column-vectors of dimensionality d
% xTe = dxm input matrix with n column-vectors of dimensionality d
% k = number of nearest neighbors to be found
% 
% Output:
% indices = kxm matrix of the where indices(i,j) is the i^th nn of xTe(:,j)
% dists = Euclidean distances to the respective nearest neighbors
%

% output random results, please erase this code
%%[d,ntr]=size(xTr);
%%[d,nte]=size(xTe);
%%indices=ceil(rand(k,nte)*ntr);
%%dists=rand(k,nte);
%%if k>ntr,k=ntr;end;

%% fill in code here
[dists_all,indices_all]=sort(l2distance(xTr,xTe));
if (size(dists_all,1)<k)
   k = size(dists_all,1); 
end
dists=dists_all(1:k,:);
indices=indices_all(1:k,:);	
%%	%
	
