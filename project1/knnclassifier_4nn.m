function preds=knnclassifier(xTr,yTr,xTe,k);
% function preds=knnclassifier(xTr,yTr,xTe,k);
%
% k-nn classifier 
%
% Input:
% xTr = dxn input matrix with n column-vectors of dimensionality d
% xTe = dxm input matrix with n column-vectors of dimensionality d
% k = number of nearest neighbors to be found
%
% Output:
%
% preds = predicted labels, ie preds(i) is the predicted label of xTe(:,i)
%


% output random result as default (you can erase this code)
%[d,n]=size(xTe);
%[d,ntr]=size(xTr);
%if k>ntr,k=ntr;end;
%un=unique(yTr);
%preds=un(ceil(rand(1,n)*length(un)));


%% fill in code here
[indices,dists]=findknn(xTr,xTe,k);
yTe=yTr(indices);
if (k==1)
    preds=yTe;
    return;
end;
%[yTe_mode,yTe_freq,yTe_all]=mode(yTe,1);
[yTe_mode,yTe_freq,yTe_all]=mode(yTe);
yTe_all_freq=cellfun('length',yTe_all);
yTe_abn=find(yTe_all_freq>1);

%knn to 1nn
%yTe_1nn=yTr(indices(1,:));
%yTe_mode(yTe_abn)=yTe_1nn(yTe_abn);
%preds=yTe_mode;

%knn to 5nn
[indices_4nn,dists_4nn]=findknn(xTr,xTe,4);
yTe_4nn=yTr(indices_4nn);
%[yTe_mode,yTe_freq,yTe_all]=mode(yTe,1);
yTe_4nn=mode(yTe_4nn);
yTe_mode(yTe_abn)=yTe_4nn(yTe_abn);
preds=yTe_mode;