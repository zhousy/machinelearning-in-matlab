function [r,s]=test()
% function [r,s]=hw1tests()
%
% Tests the functions from homework assignment 0
% Please make sure that the error statements are instructive. 
%
% Output: 
% r=0 number of tests that broke
% s= statement describing the failed test (s={} if all succeed)
%


% Put in any seed below
rand('seed',31415926535);
% initial outputs
r=0;
s={};

% Example:
% A simple test is to look if the diagonal is all zero
x=rand(10,100); 		% define some random test points
d=diag(l2distance(x));	% extract diagonal of distance matrix
if ~all(d<eps*100)			% if test failed ...
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='Distance from a vector to itself is not zero.';
end;

x=[[1,0];[0,1]];
z=[[0,0];[1,1]];
D=l2distance(x,z);
D2=[[sqrt(2),sqrt(2)];[0,0]];
if ~(max(max(abs(D2-D)))<100*eps)
	r=r+1;				% set output conditions and exit
	s{length(s)+1}='Distances between xor vectors are not correct.';
end;


%% Define your own tests here:

%test inner product: check if the inner product of two all 0 matrices is
%correct
x = zeros(2,3);
y = zeros(2,4);
G = innerproduct(x,y);
G2 = zeros(3,4);
if ~(max(max(abs(G2-G)))< 100*eps)
    r=r+1;
    s{length(s)+1} = 'Failed to create output of correct dimensions when inputs are all 0.';
end;

%test l2distance: check if 2 vectors, one the negative of the other, have 
%distance twice as great as their length (as opposed to 0)

  x = [1;23;40];
y = [-1;-23;-40];
z = [0;0;0];
x2 = [2;46;80];
if ~(l2distance(x,y) == l2distance(z,x2))
r = r+1;
  s{length(s)+1} = 'Didnt compute distance of a vector from its negative correctly';
end;

%test analyze: test that it correctly returns an accuracy of 0 when no training 
%example is correct

x = [1;2;3;4;5];
y = [2;3;4;5;1];
if ~(analyze('acc',x,y) == 0)
r = r+1;
s{length(s)+1} = 'error calculating 0% accuracy';
end;

%test findknn: check what happens when k>d (I assume that k should be set equal
					     %to d in this situation)

xTr = [1,2,3;4,5,6;7,8,9];
  xTe = [1,2,3;4,5,6;7,8,9];
if ~(findknn(xTr,xTe,4) == findknn(xTr,xTe,3))
r = r+1;
s{length(s)+1} = 'error when k>d, I check whether you handle this by setting k=d';
end;

%test knnclassifier: what happens when classes don't start at 0, and increment by 1

yTr = [1,27,83];
xTr = [1,2,3;4,5,6;7,8,9];
xTe = [1,2,3;4,5,6;7,8,9];
preds = knnclassifier(xTr,yTr,xTe,1);
if ~(preds(3) == 83)
r = r+1;
s{length(s)+1} = 'error handling classes that dont start at 0 and increment by 1';
end;